#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 22 08:05:35 2023

@author: apecher
"""

import autograd as ag
import autograd.numpy as np


from mpl_toolkits.mplot3d import axes3d
import matplotlib.pyplot as plt

def f (x,y):
    return x**3*y - 3*x**2*(y - 1) + y**2 - 1

def g (x,y):
    return x**2*y**2 - 2


# Calculés avec Maple
absc = [-1.896238074, -0.4638513741, 1.568995999]
ordo = [0.6844390650, -0.8460057970, -0.4658228710]

x = -1.5
y = -.97

fig = plt.figure(figsize = (20,10))
ax = fig.add_subplot(1, 1, 1, projection='3d')

ax.set_xlabel('$a$', labelpad=20)
ax.set_ylabel('$b$', labelpad=20)
ax.set_zlabel('$f(a,b)$', labelpad=20)

xplot = np.arange (-3.5, 1.7, 0.1)
yplot = np.arange (-2, 0.8, 0.1)

X, Y = np.meshgrid (xplot, yplot)
Z = f(X,Y)

ax.plot_surface(X, Y, Z, cmap="spring_r", lw=0.5, rstride=1, cstride=1, alpha=0.5)
ax.contour(X, Y, Z, 10, colors="k", linestyles="dashed")
ax.contour(X, Y, Z, 0, colors="blue",  levels=np.array([0], dtype=np.float64), linestyles="solid")

for i in range (len(absc)) :
    ax.scatter (absc[i], ordo[i], f(absc[i], ordo[i]), color='black')
    
 # 2ème sous-graphe

    ax = fig.add_subplot(1, 2, 2, projection='3d')

    ax.set_xlabel('$a$', labelpad=20)
    ax.set_ylabel('$b$', labelpad=20)
    ax.set_zlabel('$g(a,b)$', labelpad=20)

    xplot = np.arange (-3.5, 1.7, 0.1)
    yplot = np.arange (-2, 0.8, 0.1)

    X, Y = np.meshgrid (xplot, yplot)
    Z = g(X,Y)

    ax.plot_surface(X, Y, Z, cmap="autumn_r", lw=0.5, rstride=1, cstride=1, alpha=0.5)
    ax.contour(X, Y, Z, 10, colors="k", linestyles="dashed")
    ax.contour(X, Y, Z, 0, colors="green",  levels=np.array([0], dtype=np.float64), linestyles="solid")

    for i in range (len(absc)) :
        ax.scatter (absc[i], ordo[i], g(absc[i], ordo[i]), color='black')

    plt.show()


# On a modifié le fichier graphe_R2... de sorte à implémenter les bonnes fonctions.


#Question 2 : 
    
def Jac_F(x,y):
    
    f1 = 3*x**2*y - 6*x*(y-1)
    f2 = x**3 - 3*x**2 + 2*y
    g1 = 2*x*y**2
    g2 = x**2*2*y
    return np.array([[f1,f2],[g1,g2]],dtype=np.float64)    

#question 3

print("question 3 :")

def F(u) :
    x = u[0]
    y = u[1]
    return np.array ([x**3*y - 3*x**2*(y - 1) + y**2 - 1,x**2*y**2 - 2])

def J_F(u) :
    a = u[0]
    b = u[1]
    return np.array ([[3*a**2*b - 6*a*(b - 1), a**3 - 3*a**2 + 2*b], [2*a*b**2, 2*a**2*b]], dtype=np.float64)

u = np.array([-0.8,1.9], dtype=np.float64)

for n in range (0,7) :
    h = np.linalg.solve (- J_F(u), F(u))
    u = u + h
print(u)

u = np.array([-2.9,0.5], dtype=np.float64)

for n in range (0,7) :
    h = np.linalg.solve (- J_F(u), F(u))
    u = u + h
print(u)


#question 4 : 
    
# Fonction autograd.jacobian pour calculer la jacobienne
J_F = ag.jacobian(F)

u = np.array([-2.9,0.5], dtype=np.float64)

for n in range(0, 7): #meme calcul 
    h = np.linalg.solve(-J_F(u), F(u))
    u = u + h




# question 5 : manque de temps pour le mode forward - a revenir plus tard



#Affichage des résultats et comparaison questions 2 et 3 : 
print('jacobienne avec le module autograd')
j = ag.jacobian(F)

print(j(u))

print('jacobienne avec la fonction autograd')

print(J_F(u))


#On obtient le même résultat avec les deux première méthodes : à tester avec la question 5. 

#question 4 incomplète à revoir 








