#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec  6 08:16:36 2023

@author: apecher
"""

import autograd as ag
import autograd.numpy as np
from mpl_toolkits.mplot3d import axes3d
import matplotlib.pyplot as plt



def f (a,b):
    return a**3 + 2*a**2 - 2*a*b + b**2 + a*b**3 - 2*b + 5


def partielle_a(a,b):
    return 3*a**2+4*a-2*b+b**3

def partielle_b(a,b):
    return -2*a + 2*b + 3*a*b**2 - 2


figure = plt.figure(figsize = (20,20))
axes = plt.axes(projection='3d')

axes.set_xlabel('$a$', labelpad=20)
axes.set_ylabel('$b$', labelpad=20)
axes.set_zlabel('$f(a,b)$', labelpad=20)

aplot = np.arange (-3, 3, 0.05)
bplot = np.arange (-3, 3, 0.05)

A, B = np.meshgrid (aplot, bplot)
Z = f(A,B)
#Z = np.clip (Z, 3, 8)
Z1= partielle_a(A,B)
Z2= partielle_b(A,B)

axes.plot_surface(A, B, Z1, cmap="spring_r", lw=0.5, rstride=1, cstride=1, alpha=0.2)
axes.contour(A, B, Z1, 0, colors="blue", linestyles="solid",  levels=np.array([0], dtype=np.float64))

axes.plot_surface(A, B, Z2, cmap="spring_r", lw=0.5, rstride=1, cstride=1, alpha=0.2)
axes.contour(A, B, Z2, 0, colors="green", linestyles="solid",  levels=np.array([0], dtype=np.float64))



plt.show ()

# Q2 : NABLA 


def nabla_f(a, b):
    return np.array([partielle_a(a,b), partielle_b(a,b)])


def H_f(a, b):
    d2f_da2 = 6*a + 4
    d2f_db2 = 2 + 6*a*b
    d2f_dadb = 3*b**2 - 2
    return np.array([[d2f_da2, d2f_dadb], [d2f_dadb, d2f_db2]])


#Question 3 : 

#les points

#"(0.75,-2.2) (-1.2,-0.15)(-1.5,0.7)(0.19, 0.9) (0.22, 1.5)


 

#test des points et ecriture de newton avec nabla 

a0 = 0.75
b0 = -2.2
def newton(a0,b0):
    un = np.array ([a0,b0], dtype=np.float64)
    for i in range (6) :
         a = un[0]
         b = un[1]
         print ('u[%d] =' % i, un, 'f(u[%d]) =' % i, f(a,b))
         H_f_un = H_f (a,b) # hessienne de f en u[n]
         nabla_f_un = nabla_f (a,b) # gradient de f en u[n]
         h = np.linalg.solve (- H_f_un, nabla_f_un)
         un = un + h
    return un


print("\n Le point (0.75,-2.2) converge avec la methode de newton vers un nouveau point de coordonnées ", newton(0.75,-2.2))
print("\n Le point (-1.2,-0.15) converge avec la methode de newton vers un nouveau point de coordonnées ", newton(-1.2,-0.15))
print("\n Le point (-1.5,0.7) converge avec la methode de newton vers un nouveau point de coordonnées ", newton(-1.5,0.7))
print("\n Le point (0.19, 0.9) converge avec la methode de newton vers un nouveau point de coordonnées ", newton(0.19, 0.9))
print("\n Le point (0.22, 1.5) converge avec la methode de newton vers un nouveau point de coordonnées ", newton(0.22, 1.5))
