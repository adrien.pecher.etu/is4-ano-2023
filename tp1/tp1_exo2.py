#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 17 15:21:53 2023

@author: apecher
"""

import autograd as ag
import autograd.numpy as np
import numpy as np
import scipy.linalg as nla
import matplotlib.pyplot as plt


#On utilise la formule vue en cours : on implémente f(x) et f'(x)


def f(x):
    return x**3 - 2

def fprime(x):
    return 3 * x**2

u=2
for n in range (0, 6):
    print ('u[%d] =' % n, u)
    u = u-f(u)/fprime(u)
    
print ('Les valeurs de u convergent vers la racine cubique de 2 avec la progression des itérations')

#question 3

#données de l' : 

alpha, beta, gamma, mu = 1.5870106105525719, -3.1447447637103476, -0.37473299354311346, 7.484053576868045

# implémentation des fonctions de la partie 1

def f(x):
    return alpha*x**3 + beta*x**2 + gamma*x + mu

def fprime(x) :
    return 3*alpha*x**2 + 2*beta*x + gamma

def fseconde (x) :
    return 6*alpha*x + 2*beta

u = 0.8

for n in range (10) :
    print ('u[%d] ='% n, u)
    u = u - fprime(u)/fseconde(u)
    
print ('Les valeurs de u convergent vers le minimum local de la fonction avec la progression des itérations')

# question 4 : fonctions grad et hessian :
    
import autograd.numpy as np
from autograd import grad, hessian

alpha, beta, gamma, mu = 1.5870106105525719, -3.1447447637103476, -0.37473299354311346, 7.484053576868045

def f(x):
    return alpha*x**3 + beta*x**2 + gamma*x + mu

# Dérivée première et seconde
f_prime = grad(f)
f_seconde = hessian(f)

u = 0.8
for n in range(10):
    print('u[%d] =' % n, u)
    u = u - f_prime(u) / f_seconde(u)

print('Nous retrouvons les mêmes résultats')