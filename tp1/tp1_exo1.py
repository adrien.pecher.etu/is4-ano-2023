#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import autograd as ag
import autograd.numpy as np
import numpy as np
import scipy.linalg as nla
import matplotlib.pyplot as plt

#1 - Moindres carrés linéaires

# Fonction d
def f(x, alpha, beta, gamma, mu):
    return alpha*x**3 + beta*x**2 + gamma*x + mu

alpha=0.5
beta=-2
gamma=1
mu=7

m=4

# Points expérimentaux
Tx = np.array([-1.1, 0.17, 1.22, -0.5, 2.02, 1.81])
p = Tx.shape[0]
Ty_sur_la_courbe = np.array([f(x, alpha, beta, gamma, mu) for x in Tx])
perturbations = 0.5 * np.array([-1.3, 2.7, -5, 0, 1.4, 6])
Ty_experimentaux = Ty_sur_la_courbe + perturbations

erreur_initiale = nla.norm(Ty_sur_la_courbe - Ty_experimentaux, 2)

xplot = np.linspace(-1.2, 2.1, 100)
yplot_initiale = [f(x, alpha, beta, gamma, mu) for x in xplot]

#Tracer la courbe : 
plt.scatter(Tx, Ty_experimentaux, color='blue', label='Points expérimentaux')
plt.plot(xplot, yplot_initiale, color='blue', label='initiale')
plt.title('Courbe initiale et points expérimentaux')
plt.legend()
plt.show()

#sytème 
A = np.empty([p, 4], dtype=np.float64)
b = Ty_experimentaux

for i in range(p):
    A[i, 0] = Tx[i]**3
    A[i, 1] = Tx[i]**2
    A[i, 2] = Tx[i]
    A[i, 3] = 1

#système d'equation linéaire - résolution : 
    
AtA = np.dot(np.transpose(A), A)
Atb = np.dot(np.transpose(A), b)

# Solution optimale
x_optimal = nla.solve(AtA, Atb)

# Mise à jour des paramètres
alpha, beta, gamma, mu = x_optimal


#calcul de l'erreur minimale
Ty_optimal = np.array([f(x, alpha, beta, gamma, mu) for x in Tx])
erreur_minimale = nla.norm(Ty_experimentaux - Ty_optimal, 2)

if (erreur_minimale<erreur_initiale):
    print("l'erreur minimale est bien inférieure à l'erreur initiale")
    
yplot_optimal = [f(x, alpha, beta, gamma, mu) for x in xplot]

plt.scatter(Tx, Ty_experimentaux, color='blue', label='Points expérimentaux')
plt.plot(xplot, yplot_initiale, color='blue', label=' initiale')
plt.plot(xplot, yplot_optimal, color='orange', label='optimale')
plt.title('Courbe optimale et points expérimentaux')

