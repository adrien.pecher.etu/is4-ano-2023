Étudiant(e) : ADRIEN PECHER
CC1 = 0.0		(moyenne de la classe = 11.2)
CC2 = 12.0		(moyenne de la classe = 11.8)
CC3 = 16.0		(moyenne de la classe = 15.9)
CC4 = 10.0		(moyenne de la classe = 12.4)
CC5 = 		(moyenne de la classe = 13.1)
CC  = 11.5		(moyenne de la classe = 14.6)

CC  = la moyenne pondérée des 4 meilleures notes : les trois plus fortes 
      sont comptées avec coefficient 1, la quatrième avec un coefficient 1/3

DS  = 4.0		(moyenne de la classe = 8.8)

TP  = 6.5		(moyenne de la classe = 14)

Commentaires sur le TP1 :
-----------------------
Moindres carrés : il manque une courbe (tu as oublié un appel à plt.show) !
Par contre, la suite du TP est bien.

Commentaires sur le TP2 :
-----------------------
Tu aurais dû superposer les deux graphes sur le même graphique.
Calcul des points par Newton pas fait

Commentaires sur le TP 3 :
------------------------
Bien la technique de résolution graphique.
L’un des 5 points calculés n’est pas correct (on voit que la suite ne converge pas)
Les Q5 et Q6 ne sont pas faites
Présent(e) lors du TP 3 : non

Commentaires sur le TP 4 :
------------------------
Il manque le répertoire tp4. Push oublié ?
Présent(e) lors du TP 4 : non

Présent(e) lors du TD (TP 5) : non

